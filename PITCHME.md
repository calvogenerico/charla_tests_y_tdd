# TDD y tests Automatizados

---


## Migue

- Soy desarrollador de software de hace 4 - 5 años.                           |
- Estudiante UBA (a ritmo *tranquilo*)                                        |
- Trabajo en 10Pines como desarrollador de software                           |
- Me dijeron que tengo que poner redes sociales: Github y gitlab -> hojarasca |


---

## Un poco de contexto... Sobre ustedes.

---

- ¿Que es un objeto?
- ¿Que es un mensaje?                             |
- ¿Como se le envía un mensaje a un objeto en js? |

---

### ¿Esto es un objeto?

``` javascript
function saludar() {
  console.log('hello world');
}
```
---

* ¿Cual es la fuente de todos los males en la programación?

---

- ¿Que es el "diseño" con objetos?
- ¿Que propiedades nos gusta que tengan nuestos objetos?  |
- ¿Como creamos objetos en javascript?                    |
- ¿Que es el encapsulamiento? ¿Para que sirve?            |
- ¿Que es la cohesión? ¿Que es el acoplamiento            |


---

### ¿Por qué tantas preguntas?

---

### Ahora si... volvamos a los tests

---

### .... Pero antes una historia

---

Resulta que estamos manteniendo una aplicación...
Una aplicación vieja, con código muy legacy (viejo, feo y olvidado).
---

#### Claramente no lo escribimos nosotros

![face with rolling eyes](https://emojipedia-us.s3.amazonaws.com/thumbs/120/apple/96/face-with-rolling-eyes_1f644.png)


---

### En cierto momento encontramos este código:

``` javascript
usuario() {
  //..
  //--

  /**
   *devuelve verdadero solamente si *unaFecha* es el cumpleanios del usuario
   */
  esLaFechaDeCumpleanios(unaFecha)

  //..
}
```

---

### Mas adelante encontramos esto...

``` javascript
  var fecha1 = usuario.fechaNacimiento;
  //..
  //..
  if (fecha1 === fecha2) {
    //..
  }
```

---

### Hay una oportunidad de **mejora**.

---

``` javascript
  var fecha1 = usuario.fechaNacimiento;
  //..
  //..
  if (fecha1 === fecha2) {
    //..
  }
```

---

``` javascript
  //..
  if (usuario.esLaFechaDeCumpleanios(unaFecha)) {
    //...
  }
  //..
```
---

``` javascript
  //..
  if (usuario.esLaFechaDeCumpleanios(unaFecha)) {
    //...
  }
  //..
```

- Sacamos código viejo
- Reutilizamos cósigo existente... |
- Abrimos la aplicación y miramos que la funcionalidad sigue andando. |
- ¿Que podría salir mal?           |

---

### Todo.

---

![kaboom](http://www.clipartsfree.net/vector/large/kaboom_Vector_Clipart.png)

---

### La trampa:

``` javascript
esLaFechaDeCumpleanios(fecha) {
  return this.fechaNacimiento == fecha;
}
```

---

### ¿Que es lo que causó el problema?

- Hicimos un cambio muy inocente |
- Nuestro cambio mejoraba la base código |
- Abrimos el navegador y nos fijamos que ande |
- Sin embargo... hubo problemas |


---

### El problema fue que cuando hicimos nuestras pruebas manuales faltó testear un caso.

---

### Posiblemente era imposible que testiemos ese caso porque no sabíamos que podía pasar.

---

Esta es una situación en sistemas muy grandes. Sobre todo cuando uno
tiene poco conocimiento del sistema o cuando los desarrolladores
originales ya no están disponibles.

---

## Y ahora si... Vamos a los tests... Depsués de la siguiente historia


---

### Nuestra situación mejoró. Estamos e una apliación con una base de código mejor

---

### El código es nuevo, todos los desarrolladores que empezaron están en el proyecto.

---

### Y es el día del despliegue*.



---

### Minutos antes del despligue encontramos un pequeñisimo bug

- La solución es muy sencila |
- Si lo arreglamos nos vamos a ahorrar tener que volver a hacer un despligue |
- Pero... ¿Y si ese pequeño cambio produce algún problema en otro pedazo de la aplicación que no estamos mirando? |


---

## Que miedito...

![mauricio asustado](http://i0.kym-cdn.com/photos/images/newsfeed/000/438/093/ee8.jpg)

---

## Para todas estas situaciones y mas también... Tests automatizados al rescate.

---

### ¿Cuál es el problema que identificamos?

- En general es dificil medir el impacto de cambiar código. |
- No hay documetación que cobra realmente todos los casos. |
- El código puede ser engañoso, sobre todo cuando es viejo y olvidado |


---

### Necesitamos una herramienta que nos ayude a tener confianza a la hora de cambiar el código.

---

### Tests automatizados al rescate.


---

### ¿Que es una suite de tests automatizados?

- Es un programa que corre pruebas sobre nuestra aplicacion y verifica que ande bien. |
- Este programa también lo desarrollamos nosotros, usando herramientas que facilitan su desarrollo. |
- Corre rápido y es fácil de usar. Además provee buen feedback. |

---

## Lo importante de los tests... no es que testean, es que son automáticos.

---

- La suite de tests se corre TODO el tiempo.
- Por eso tiene correr rápido. |
- Y tiene que ser fácil correrla. |
- Incluso no está mal hacer que un robot la corra por nosotros todos cada vez que hay un cambio, para evitar accidentes. |

---

Cuando existe una buena base de tests... Uno programa mas tranquilo.
Uno tiene cierto nivel de confianza de que sus cambios no están rompiendo
nada pre existente.

---

Mientras mas grande es la suite de tests mas confianza tiene el desarrollador
mientras desarrolla nueva funcionalidad, y mas tempranamente se pueden
enconrar errores.

---

Cuando no hay tests... Se programa incómodo.


---

## Bajando a la tierra...

---


## Escribamos algunos tests para la función de fibonacci.

---

- `fibonaccio(0) === 0` |
- `fibonaccio(1) === 1` |
- `fibonaccio(n) === fibnaccio(n-2) + fibonaccio(n-1)` |

---

### Demo y esas cositas...

---

# `console.log` no alcanza


---

### Frameworks!

- QUnit |
- Mocha * |
- Chai  * |

---


### Reescribamos nuestros tests usando mocha...


---

### Usar le proyector para usar mocha todos juntos.


---


# TDD

al fin...


---

## ¿Que es?

- ¿Para que sirve? |
- ¿Por qué quiero usarlo? |

---

## Metodología

- Testear (test) |
- Codear (code) |
- Mejorar (refactor) |

---

## ¿No es mas lento codear así?

- Al fin y al cabo... tengo que tocar mas teclas |
- Tengo que pensar mas cosas |
- Tengo que usar mas tecnologías |

---

## Cabio de paradigma

- Los tests son *parte* del código |
- Podemos **medir** nuestro código en tests. |
- Podemos obtener nuevas métricas. |


---

### TDD se aprende con los dedos... A Codear.

---

## Ejercicio 2: Pila

![imagen pila](https://www.cs.cmu.edu/~adamchik/15-121/lectures/Stacks%20and%20Queues/pix/stack.bmp)

---

Vamos a construir una estructura de datos del tipo "pila" usando tdd.
Vamos a usar Mocha y vamos a usar Chai.


---
# Veamos que sale...
---


## ¿Que onda TDD?

---

- ¿Como se sintió? |
- ¿Valió la pena? |
- ¿Se ven haciendo TDD en el futuro? |

---

### Por otro lado tdd...

- ¿Nos ayuda a programar mejor? |
- ¿Nos impone reglas de diseño? |

---

### TDD nos ayuda a crear diseños testeables... Lo cual no significa que sean buenos obligatoriamente.

---

### Ahora que sabemos tdd nos la vamos a pasar escribiendo tests, entonces...

---

## Necesitamos aprender a escribir *buenos* tests.

---

# ¿Como es un test de esos que te enamoran?

---

- Testea UN y solo UN caso |
- Es claro |
- Tiene las aserciones al final y solo al final |
- Inicializa el contexto al principio y solo al principio |
- Es claro otra vez |

---

## Pero además...

- Es muy claro |
- No rompe el encapsulamiento (test de caja negra) |
- Corre muy rápido |
- Es mucho muy claro y expresivo |
- Es determinístico |

---

- Testea un `caso` no un `ejemplo`
- Usa bien los features del framework |
- Es muy muy claro |

---

## Por otro lado una buena `suite` de tests...

- Es exhaustiva |
- Testa casos positivos... y casos negativos. |
- No repite casos. |
- Es clara y declarativa. |
- Usa bien los features del framework. |

---

## Un último ejercicio...

---


Se quiere hacer un 'ecommerce' para una página de una juguetería.
Solo usuarios logueados pueden hacer compras en la juguetería. Cada usuario
puede tener un solo carrito a la vez. Ese carrito puede tener todosl los
productos que el usuario elija, y luego se usa para hacer el cobro.
Es necesario que el usuario pueda elegir, borrar o modificar los productos
que seleccionó y que además pueda saber cuanto lleva gastado en total.  


---

### Un poco de análisis...

- Este parrafito, si bien de manera medio oscura, nos está hablando de un "carrito". |
- ¿Que tiene que permitirnos hacer este carrito? |
- ¿Cual sería el *primer* tests? |
